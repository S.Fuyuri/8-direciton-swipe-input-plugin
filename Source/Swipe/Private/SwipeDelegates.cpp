//
//  Created by Derek van Vliet on 2014-12-10.
//  Copyright (c) 2015 Get Set Games Inc. All rights reserved.
//

#include "SwipeDelegates.h"
#include "ISettingsModule.h"

USwipeDelegates::FTouchDelegate USwipeDelegates::TouchBeganDelegate;
USwipeDelegates::FTouchDelegate USwipeDelegates::TouchMovedDelegate;
USwipeDelegates::FTouchDelegate USwipeDelegates::TouchEndedDelegate;

USwipeDelegates::FSwipeTriggeredDelegate USwipeDelegates::SwipeLeftDelegate;
USwipeDelegates::FSwipeEndedDelegate USwipeDelegates::SwipeLeftEndedDelegate;

USwipeDelegates::FSwipeTriggeredDelegate USwipeDelegates::SwipeUpperLeftDelegate;
USwipeDelegates::FSwipeEndedDelegate USwipeDelegates::SwipeUpperLeftEndedDelegate;

USwipeDelegates::FSwipeTriggeredDelegate USwipeDelegates::SwipeLowerLeftDelegate;
USwipeDelegates::FSwipeEndedDelegate USwipeDelegates::SwipeLowerLeftEndedDelegate;

USwipeDelegates::FSwipeTriggeredDelegate USwipeDelegates::SwipeRightDelegate;
USwipeDelegates::FSwipeEndedDelegate USwipeDelegates::SwipeRightEndedDelegate;

USwipeDelegates::FSwipeTriggeredDelegate USwipeDelegates::SwipeUpperRightDelegate;
USwipeDelegates::FSwipeEndedDelegate USwipeDelegates::SwipeUpperRightEndedDelegate;

USwipeDelegates::FSwipeTriggeredDelegate USwipeDelegates::SwipeLowerRightDelegate;
USwipeDelegates::FSwipeEndedDelegate USwipeDelegates::SwipeLowerRightEndedDelegate;

USwipeDelegates::FSwipeTriggeredDelegate USwipeDelegates::SwipeUpDelegate;
USwipeDelegates::FSwipeEndedDelegate USwipeDelegates::SwipeUpEndedDelegate;

USwipeDelegates::FSwipeTriggeredDelegate USwipeDelegates::SwipeDownDelegate;
USwipeDelegates::FSwipeEndedDelegate USwipeDelegates::SwipeDownEndedDelegate;

USwipeDelegates::FTapDelegate USwipeDelegates::SingleTapDelegate;
USwipeDelegates::FTapDelegate USwipeDelegates::DoubleTapDelegate;
