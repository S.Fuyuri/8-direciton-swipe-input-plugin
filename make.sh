#$1 = RunUAT.sh fullpath
#$2 = Swipe.uplugin fullpath
#$3 = platform
echo "Full path to RunUAT.sh?"
read RUNUAT_PATH
echo "Path to the project directory?"
read PROJECT_PATH

$RUNUAT_PATH BuildPlugin -Plugin=`pwd`/Swipe.uplugin -Package=$PROJECT_PATH/Plugins/Swipe -TargetPlatforms=Linux
